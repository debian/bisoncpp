#define XERR
#include "scanner.ih"

void Scanner::returnTypeSpec()
{
    string trimmed = String::trim(d_typeName);
    
    push(matched());      // return '%' or ';' for renewed scanning

    trimmed.resize(trimmed.length());
    
    setMatched(String::trim(trimmed));

    d_typeName.clear();

    begin(StartCondition_::INITIAL);

    leave(Tokens::IDENTIFIER);
}
