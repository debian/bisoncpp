#include "generator.ih"

ofstream Generator::tokenPath() const
{
    error_code ec;

    path tokenPath{ d_options.tokenPath()  };

    path parent = tokenPath.parent_path();

    if (not parent.empty())
        create_directories(parent, ec);

    if (ec.value() != 0)
        throw Exception{} << "cannot create path to `" <<
                             tokenPath.string() << '\'';

    ofstream ret = Exception::factory<ofstream>(d_options.tokenPath());

    string stem = String::uc(tokenPath.stem().string());

    ret << "#ifndef INCLUDED_" << stem << "_\n"
           "#define INCLUDED_" << stem << "_\n"
           "\n";

    if (string const &ns = d_options.tokenNameSpace(); not ns.empty())
        ret <<  "namespace " << ns << "\n"
                "{\n"
                "\n";

    ret <<  "struct " << d_options.tokenClass() << "\n"
            "{";

    return ret;
}
