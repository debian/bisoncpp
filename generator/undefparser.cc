#include "generator.ih"

void Generator::undefparser(ostream &out) const
{
    if (d_options.useTokenPath())
        return;
    
    key(out);
    insert(out, 0, "undef.in");
}
