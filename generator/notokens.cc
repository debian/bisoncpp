#include "generator.ih"

namespace 
{
    char defineParserBase[] = R"(
// hdr/tail
// For convenience, when including ParserBase.h its symbols are available as
// symbols in the class Parser.
#define )";

}

void Generator::notokens(std::ostream &out) const
{
    if (d_options.useTokenPath())
        return;

    string const &className = d_options.className();

    out << defineParserBase << className << ' ' << className << "Base\n"
           "\n";
}
