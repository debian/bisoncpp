
inline void StateItem::setLA(LookaheadSet const &laSet)
{
    d_LA = laSet;
}

inline void StateItem::addProduction(Production const *production, 
                                     StateItem::Vector &stateItem)
{
    stateItem.push_back(StateItem(Item(production)));
}

inline LookaheadSet const &StateItem::lookaheadSet() const
{
    return d_LA;
}

inline size_t StateItem::lookaheadSetSize() const
{
    return d_LA.fullSize();
}

inline bool StateItem::isReducible() const
{
    return d_item.isReducible();
}

inline Symbol const *StateItem::symbolAtDot() const
{
    return d_item.dotSymbolOr0();
}

inline Symbol const *StateItem::lhs() const
{
    return d_item.lhs();
}

inline void StateItem::setNext(size_t next)
{
    d_nextIdx = next;
}

inline size_t StateItem::next() const
{
    return d_nextIdx;
}

inline Item const &StateItem::item() const
{
    return d_item;
}

inline Symbol const *StateItem::precedence() const
{
    return d_item.production()->precedence();
}

inline size_t StateItem::nr() const
{
    return d_item.production()->nr();
}

inline void StateItem::removeRRConflict(RRData const &rm,
                                       Vector &itemVector)
{
    itemVector[rm.reduceIdx()].d_LA -= rm.lookaheadSet();
}

inline void StateItem::removeReduction(RmReduction const &rm,
                                       Vector &itemVector)
{
    itemVector[rm.idx()].d_LA -= rm.symbol();
}

inline Production const *StateItem::production() const
{
    return d_item.production();
}

inline bool StateItem::lookaheadContains(StateItem const &stateItem, 
                                                   Symbol const *symbol)
{
    return stateItem.d_LA >= symbol;
}

inline void StateItem::inserter(std::ostream &(StateItem::*insertPtr)
                                         (std::ostream &out) const)
{
    s_insertPtr = insertPtr;
}


inline std::ostream &operator<<(std::ostream &out, StateItem const &stateItem)
{
    return (stateItem.*StateItem::s_insertPtr)(out);
}
