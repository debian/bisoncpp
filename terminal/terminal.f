inline std::ostream &operator<<(std::ostream &out, 
          std::ostream &(Terminal::*insertPtr)(std::ostream &out) const)
{
    Terminal::inserter(insertPtr);
    return out;
}

inline std::ostream &Terminal::plainName(std::ostream &out) const
{
    return out << d_readableLiteral;
}

inline Terminal *Terminal::downcast(Symbol *sp)
{
    return dynamic_cast<Terminal *>(sp);
}

inline Terminal const *Terminal::downcast(Element const *sp)
{
    return dynamic_cast<Terminal const *>(sp);
}

inline void Terminal::incrementPrecedence()
{
    ++s_precedence;
}

inline void Terminal::resetPrecedence()     // see Parser::parseDeclarations()
{
    s_precedence = 0;
}

inline size_t Terminal::maxValue()
{
    return s_maxValue;
}

inline Terminal::Association Terminal::association() const
{
    return d_association;
}

inline Terminal::Precedence Terminal::comparePrecedence(
                                                Terminal const *other) const
{
    return 
        d_precedence > other->d_precedence ? LARGER  :
        d_precedence < other->d_precedence ? SMALLER :
                                         EQUAL;
}            

inline size_t Terminal::precedence() const
{
    return d_precedence;
}

inline size_t Terminal::v_value() const
{
    return d_value;
}

inline FirstSet const &Terminal::v_firstSet() const
{
    return d_firstSet;
}        

inline void Terminal::setLiteral(std::string const &literal)
{
    d_literal = literal;
}

inline void Terminal::setPrecedence(size_t value)
{
    d_precedence = value;
}

inline size_t Terminal::sPrecedence()
{
    return s_precedence;
}

inline void Terminal::set_sPrecedence(size_t prec)
{
    s_precedence = prec;
}

inline bool Terminal::compareValues(Terminal const *left, 
                                    Terminal const *right)
{
    return left->d_value < right->d_value;
}

inline std::ostream &Terminal::insert(std::ostream &out) const
{
    return (this->*Terminal::s_insertPtr)(out);
}

inline void Terminal::inserter(std::ostream &(Terminal::*insertPtr)
                                             (std::ostream &out) const)
{
    s_insertPtr = insertPtr;
}
