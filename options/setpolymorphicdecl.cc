#include "options.ih"

// define the polymorphic semantic value type, embedded in a shared_ptr

void Options::setPolymorphicDecl()
{
    if (isFirstStypeDefinition())
        d_stackDecl = "    using STYPE_ = Meta_::SType;\n";

    d_polymorphic = true;
}

