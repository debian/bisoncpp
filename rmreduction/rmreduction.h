#ifndef _INCLUDED_RMREDUCTION_
#define _INCLUDED_RMREDUCTION_

#include <vector>
#include <cstddef>

class Symbol;

class RmReduction
{
    public:
        using Vector = std::vector<RmReduction>;
        using ConstIter = Vector::const_iterator;

    private:
        size_t  d_idx;              // idx in a StateItem::Vector of
                                    // reduce-production 
        size_t  d_next;             // next state when shifting
        Symbol const *d_symbol;     // symbol causing the S/R conflict
                                    
        bool   d_forced;            // forced if not based on precedence or
                                    // associativity 
    public:
        RmReduction() = default;    // Only needed for vectors
        RmReduction(size_t idx, size_t next, 
                    Symbol const *symbol, bool forced);

        size_t idx() const;
        size_t next() const;
        Symbol const *symbol() const;

        static bool isForced(RmReduction const &rmReduction);
};

#include "rmreduction.f"

#endif
